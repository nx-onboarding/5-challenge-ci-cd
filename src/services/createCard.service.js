const dynamodb = require('ebased/service/storage/dynamo');

const createCardService = async (payload, tableName = null) => {
  const { dni, ccv, expirationDate, cardNumber, type } = payload;

  const dbParams = {
    TableName: tableName || process.env.CLIENTS_TABLE,
    ExpressionAttributeNames: {
      '#C': 'creditCard',
    },
    ExpressionAttributeValues: {
      ':c': {
        ccv,
        expirationDate,
        cardNumber,
        type,
      },
    },
    UpdateExpression: 'SET #C = :c',
    ReturnValues: 'ALL_NEW',
    Key: {
      dni,
    },
  };

  await dynamodb.updateItem(dbParams);
};

module.exports = { createCardService };
