const dynamodb = require('ebased/service/storage/dynamo');

const createClientService = async (payload, tableName = null) => {
  await dynamodb.putItem({
    TableName: tableName || process.env.CLIENTS_TABLE,
    Item: payload,
  });
};

module.exports = { createClientService };
