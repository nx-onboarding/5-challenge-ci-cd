const { CreateClientSchema } = require('../schemas/inputs/createClient.schema');
const { ClientCreatedSchema } = require('../schemas/events/clientCreated.schema');
const { createClientService } = require('../services/createClient.service');
const { snsCreateClientService } = require('../services/snsCreateClient.service');
const { existClientService } = require('../services/existClient.service');
const { validateAge } = require('../helpers');

module.exports = async (commandPayload, commandMeta) => {
  const client = new CreateClientSchema(commandPayload, commandMeta).get();
  await existClientService(commandPayload);
  validateAge(commandPayload);
  await createClientService(commandPayload);
  await snsCreateClientService(new ClientCreatedSchema(commandPayload, commandMeta));

  return {
    statusCode: 200,
    body: JSON.stringify({
      client,
      message: 'Client successfully registered',
    }),
  };
};
