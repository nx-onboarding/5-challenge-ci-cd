const { numberByLength, numberRnd, typeCardByAge } = require('../helpers');
const { CreateCardSchema } = require('../schemas/inputs/createCard.schema');
const { createCardService } = require('../services/createCard.service');

module.exports = async (eventPayload, eventMeta) => {
  const { Message } = eventPayload;
  let client = new CreateCardSchema(JSON.parse(Message), eventMeta).get();

  client = {
    ...client,
    ccv: numberByLength(3),
    expirationDate: `${numberRnd(1, 12)}/${numberRnd(23, 33)}`,
    cardNumber: numberByLength(16),
    type: typeCardByAge(client.birthDate),
  };

  await createCardService(client);

  return {
    statusCode: 200,
    body: JSON.stringify({
      client,
      message: 'Card successfully registered',
    }),
  };
};
